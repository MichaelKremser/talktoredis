const path = require('path');
const fs = require('fs');

module.exports = {
    getFileContentFromAnyDirectory: function(filename, directoriesToTry, log) {
        var filepath;
        var content;
        var result;
        for (let directoryToTry of directoriesToTry) {
            filepath = path.join(directoryToTry, filename);
            log("trying " + filepath);
            try {
                content = fs.readFileSync(filepath);
                result = true;
            }
            catch {
                result = false;
            }
            if (result) {
                log("found " + filepath);
                return content;
            }
        }
        return "";
    }
}