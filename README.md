# talktoredis

In February 2013 I was curious to learn about Redis and NodeJS, hence I tried to find a small, simple project idea that I could realize using these technologies. So on one evening I managed to write a Node JS app that uses Redis to send a message to a bash script. Be aware: I didn't had code quality and software engineering best practices in mind, instead I wanted to learn these technologies and do something with it in two hours. As a consequence, there are some flaws. The Python script was developed some years later because I was sure that it was better to do this job with Python than with a Bash script.

The project consists of three parts which I would like to explain:

## talktoredis.js

This is a Node JS app that listens on port 8443 using TLS and waits for a user to authenticate. Then the user can click on a link which makes the app writing a predefined value to a Redis server.

### Installation

To install required npm packages, use

```
npm install
```

### Configuration

Create file /etc/talktoredis.json with this content:

```
{
	"username": "", /* username for authentication */
	"password" : "", /* password for authentication */
	"options" : {
	  "hostname" : "", /* currently not in use, can be skipped or kept blank */
	  "port" : 443, /* change this if using default HTTPS port is inappropriate */
	  "key" : "file:", /* TLS: path to the private key (a file that starts with -----BEGIN PRIVATE KEY-----) */
	  "cert" : "file:/etc/ssl/certs/speedy-cert.pem" /* TLS: path to the certificate (a file that starts with -----BEGIN CERTIFICATE-----) */
	},
	"wlan_powermode_redirect_uri" : "", /* URI that should be used for forwarding after WLAN state has changed */
    "webcam_powermode_redirect_uri" : "", /* URI that should be used for forwarding after webcam state has changed */
	"webpage_template" : "/media/Daten1/Code/GitLab/talktoredis/examples/talktoredis.html"
}
```

## start_talktoredis

Starts the beforementioned Node JS app.

## checkrediskey

This is a shell script that polls the values of some keys of the Redis server in a specified interval. It reacts to changes and executes commands dependent on the key whose value was changed.

## CheckRedisKey.py

Does basically the same as its shell script counterpart checkrediskey. The first difference is the syscmd "log", which just prints the date and time instead of writing it to some file in "/root" (which can be considered quite bad practice). The second difference is that it features a syscmd "restart_nm" to restart network manager.

This script requires Python package `redis` and an available Redis instance (see package `redis-server` on Debian).

### Installation

Package `python3-venv` might be required for this to work. It is strongly recommended to install a virtual environment for this script.

```
python3 -m venv env
env/bin/pip install -r requirements.txt
```

# Demonstration video

I've uploaded a demonstration video on YouTube:

https://www.youtube.com/watch?v=cEb_Q4gV6x0

# Future improvements

## talktoredis.js

All the settings should be read from a configuration file rather than being hard coded.

## CheckRedisKey.py

The commands to execute should be in some configuration file rather than being hard coded.
