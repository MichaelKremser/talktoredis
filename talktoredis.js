const https = require('https');
const fs = require('fs');
const path = require('path');
const redis = require('redis'),
        client = redis.createClient();
const url = require('url');
const lib = require('./libtalktoredis');
const { exit } = require('process');
const appId = 'talktoredis 0.2.6';

function logWithDateTime(msg) {
	console.log(new Date().toISOString().replace('T', ' ') + ': ' + msg);
}

async function getKeyDefensive(key) {
  var value = await client.get(key);
  if (value === undefined) {
    value = "";
  }
  if (value === null) {
    value = "";
  }
  return value;
}

logWithDateTime(`${appId} initializing...`);
try {
var settings = JSON.parse(
    lib.getFileContentFromAnyDirectory('talktoredis.json', [ "/etc", __dirname ], console.log)
    );
}
catch {
  console.error("Could not load configuration file!");
  exit(1);
}
if (settings.options.key.startsWith("file:")) {
  var keyFilename = settings.options.key.substr(5);
  logWithDateTime('key file for TLS is ' + keyFilename);
  settings.options.key = fs.readFileSync(keyFilename);
}
if (settings.options.cert.startsWith("file:")) {
  var certFilename = settings.options.cert.substr(5);
  logWithDateTime('cert file for TLS is ' + certFilename);
  settings.options.cert = fs.readFileSync(certFilename);
}

logWithDateTime("Connecting to redis...");
client.connect();
logWithDateTime(`${appId} started!`);

https.createServer(settings.options, async function (req, res) {
  var powermode = '';
  query = url.parse(req.url,true).query;
  var header = req.headers['authorization']||'',        // get the header
  token = header.split(/\s+/).pop()||'',            // and the encoded auth token
  auth = Buffer.from(token, 'base64').toString(),    // convert from base64
  parts=auth.split(/:/),                          // split on colon
  username=parts[0],
  password=parts[1];
  logWithDateTime('Expecting username ' + settings.username);
  var remoteAddress = req.socket.remoteAddress;
  if (username === settings.username && password === settings.password) {
    if (query.newvalue == 'on' || query.newvalue == 'off' || query.newvalue == 'reset' || query.newvalue == 'reset10' || query.newvalue == 'reset60') {
      client.set("wlan.powermode", query.newvalue);
      logWithDateTime('Host ' + remoteAddress + ' changed wlan state to ' + query.newvalue);
      res.writeHead(302, {'Location': settings.wlan_powermode_redirect_uri});
      res.write("You're going to be redirected to the log...");
      res.end();
    }
    else {
      if (query.newvalue == 'webcamon' || query.newvalue == 'webcamoff') {
        client.set("webcam.powermode", query.newvalue);
        logWithDateTime('Host ' + remoteAddress + ' changed webcam state to ' + query.newvalue);
        res.writeHead(302, {'Location': settings.webcam_powermode_redirect_uri});
        res.write("You're going to be redirected to the webcam picture...");
        res.end();
      }
      else {
        logWithDateTime('Sending web page');
        res.writeHead(200, {'Content-Type': 'text/html'});

        var template = fs.readFileSync(settings.webpage_template).toString();
        var content = "";
        
        content += '<h2>Turning on and off devices</h2>';
        
        powermode = await getKeyDefensive('wlan.powermode');
        logWithDateTime('wlan.powermode = ' + powermode);
        logWithDateTime('Sending wlan options to ' + remoteAddress);
        res.write('<h3>WLAN</h3>');
        const isWlanOn = (powermode !== "off" && powermode !== "");
        if (!isWlanOn) {
          content += '<a href="?newvalue=on">Turn WLAN on</a>';
        }
        else {
          content += '<a href="?newvalue=off">Turn WLAN off</a> <br />';
          content += '<a href="?newvalue=reset">Reset WLAN</a><br />';
          content += '<a href="?newvalue=reset10">Reset WLAN (wait 10 sec between stop and start)</a><br />';
          content += '<a href="?newvalue=reset60">Reset WLAN (wait 60 sec between stop and start)</a><br />';
        }
        
        powermode = await getKeyDefensive("webcam.powermode");
        logWithDateTime('webcam.powermode = ' + powermode);
        logWithDateTime('Sending wlan options to ' + remoteAddress);
        content += '<h3>Webcam</h3>';
        const isWebcamOn = (powermode !== "off" && powermode !== "");
        if (!isWebcamOn) {
          content += '<a href="?newvalue=webcamon">Turn webcam on</a>';
        }
        else {
          content += '<a href="?newvalue=webcamoff">Turn webcam off</a>';
        }

        logWithDateTime('Finished');

        template = template.replace("{content}", content);
        res.write(template);
        res.end();
        }
    }
  }
  else {
    logWithDateTime('Auth failed for ' + remoteAddress);
    res.writeHead(401, {'Content-Type': 'text/html', 'WWW-Authenticate': 'Basic realm="noderedis"' });
    res.write('<html><head><title>Wireless LAN Power Management</title></head>');
    res.write('<body>401 Unauthorized. This incident has been logged. Logs are contiuously supervised by men.</body></html>');
    res.end();
  }
}).listen(settings.options.port);
